# Usage

## Run tests
go test ./... -v

## Run benchmarks
go test ./... -bench=. -benchtime=30s
