package integration

import (
	"fmt"
	"math"
	"reflect"
	"runtime"
	"strings"
	"testing"
)

// Constant functions
func one(x float64) float64 {
	return 1
}

func two(x float64) float64 {
	return 2
}

// Linear functions
func twoX(x float64) float64 {
	return 2 * x
}

// Polynomial functions
func xSquared(x float64) float64 {
	return math.Pow(x, 2)
}

func xCubed(x float64) float64 {
	return math.Pow(x, 3)
}

// Exponential functions
func twoToX(x float64) float64 {
	return math.Pow(2, x)
}

func GetFunctionName(i interface{}) string {
	base := runtime.FuncForPC(reflect.ValueOf(i).Pointer()).Name()
	foo := strings.Split(base, ".")
	return foo[len(foo)-1]
}

var integrationCases = []struct {
	f       euclidianFunc
	from    float64
	to      float64
	slices  int
	want    float64
	epsilon float64
}{
	{f: one, from: -10, to: 0, slices: 8, want: 10.0, epsilon: 1e-4},
	{f: one, from: -10, to: 0, slices: 100, want: 10.0, epsilon: 1e-4},
	{f: one, from: 0, to: 10, slices: 8, want: 10.0, epsilon: 1e-4},
	{f: one, from: 0, to: 10, slices: 100, want: 10.0, epsilon: 1e-4},

	{f: two, from: -10, to: 0, slices: 8, want: 20.0, epsilon: 1e-4},
	{f: two, from: -10, to: 0, slices: 100, want: 20.0, epsilon: 1e-4},
	{f: two, from: 0, to: 10, slices: 8, want: 20.0, epsilon: 1e-4},
	{f: two, from: 0, to: 10, slices: 100, want: 20.0, epsilon: 1e-4},

	{f: twoX, from: 0, to: 10, slices: 100, want: 100.0, epsilon: 1e-4},
	{f: twoX, from: 0, to: 10, slices: 1000, want: 100.0, epsilon: 1e-4},

	{f: xSquared, from: 0, to: 10, slices: 100, want: 333.333, epsilon: 1e-4},
	{f: xSquared, from: 0, to: 10, slices: 1000, want: 333.333, epsilon: 1e-4},

	{f: xCubed, from: 0, to: 10, slices: 100, want: 2500.0, epsilon: 1},
	{f: xCubed, from: 0, to: 10, slices: 1000, want: 2500.0, epsilon: 1e-4},
	{f: xCubed, from: 0, to: 10000, slices: 1000, want: 2500000000000000, epsilon: 1e-4},

	{f: twoToX, from: 0, to: 10, slices: 1000, want: 1475.9, epsilon: 1e-4},
	{f: twoToX, from: 0, to: 32, slices: 1000000, want: 6.19632801727679e9, epsilon: 1e-4},
}

func TestIntegrateSequential(t *testing.T) {
	for _, af := range []string{"trap", "midp", "simp"} {
		for _, tc := range integrationCases {
			fName := GetFunctionName(tc.f)
			t.Run(fmt.Sprintf("f:%v[%.1f,%.1f] af:%v s:%d",
				fName, tc.from, tc.to, af, tc.slices),
				func(t *testing.T) {
					res := Integrate(tc.f, af, tc.from, tc.to, tc.slices, 1)
					if !approximately(tc.want, res, tc.epsilon) {
						t.Errorf(
							"Wrong output\nwanted: %v\n   got: %v\n  diff: %v\n     e: %v",
							tc.want, res, math.Abs(tc.want-res), tc.epsilon)
					}
				})
		}
	}
}

var defaultWorkers = 8

func TestIntegrateParallel(t *testing.T) {
	for _, af := range []string{"trap", "midp", "simp"} {
		for _, tc := range integrationCases {
			fName := GetFunctionName(tc.f)
			t.Run(fmt.Sprintf("f:%v[%.1f,%.1f] af:%v s:%d",
				fName, tc.from, tc.to, af, tc.slices),
				func(t *testing.T) {
					resPar := Integrate(tc.f, af, tc.from, tc.to, tc.slices, defaultWorkers)
					if !approximately(tc.want, resPar, tc.epsilon) {
						t.Errorf(
							"Wrong output\nwanted: %v\n   got: %v\n  diff: %v\n     e: %v",
							tc.want, resPar, math.Abs(tc.want-resPar), tc.epsilon)
					}
				})
		}
	}
}

func TestSimpsonCoefficents(t *testing.T) {
	var cases = []struct {
		a    int
		want []float64
	}{
		{a: 1, want: []float64{1}},
		{a: 3, want: []float64{1, 4, 1}},
		{a: 5, want: []float64{1, 4, 2, 4, 1}},
		{a: 7, want: []float64{1, 4, 2, 4, 2, 4, 1}},
		{a: 19, want: []float64{1, 4, 2, 4, 2, 4, 2, 4, 2, 4, 2, 4, 2, 4, 2, 4, 2, 4, 1}},
	}

	for _, tc := range cases {
		t.Run(fmt.Sprintf("%v = %v", tc.a, tc.want),
			func(t *testing.T) {
				res := simpsonCoefficients(tc.a)
				if !reflect.DeepEqual(res, tc.want) {
					t.Errorf(
						"Wrong output for %v\nwanted: %v\n   got: %v\n",
						tc.a, tc.want, res)
				}
			})
	}
}

var benchIntFrom = float64(0)
var benchIntTo = float64(1000000)
var benchIntSlicesLarge = 10000000
var benchIntSlicesMedium = 1000
var benchIntSlicesSmall = 10

func BenchmarkIntegrationSimpSequential1e7(b *testing.B) {
	for i := 0; i < b.N; i++ {
		Integrate(twoToX, "simp", benchIntFrom, benchIntTo, benchIntSlicesLarge, 1)
	}
}

func BenchmarkIntegrationSimpParallel1e7(b *testing.B) {
	for i := 0; i < b.N; i++ {
		Integrate(twoToX, "simp", benchIntFrom, benchIntTo, benchIntSlicesLarge, 8)
	}
}

func BenchmarkIntegrationTrapSequential1e7(b *testing.B) {
	for i := 0; i < b.N; i++ {
		Integrate(twoToX, "trap", benchIntFrom, benchIntTo, benchIntSlicesLarge, 1)
	}
}

func BenchmarkIntegrationTrapParallel1e7(b *testing.B) {
	for i := 0; i < b.N; i++ {
		Integrate(twoToX, "trap", benchIntFrom, benchIntTo, benchIntSlicesLarge, 8)
	}
}

func BenchmarkIntegrationMidpSequential1e7(b *testing.B) {
	for i := 0; i < b.N; i++ {
		Integrate(twoToX, "midp", benchIntFrom, benchIntTo, benchIntSlicesLarge, 1)
	}
}

func BenchmarkIntegrationMidpParallel1e7(b *testing.B) {
	for i := 0; i < b.N; i++ {
		Integrate(twoToX, "midp", benchIntFrom, benchIntTo, benchIntSlicesLarge, 8)
	}
}

func BenchmarkIntegrationSimpSequential1e3(b *testing.B) {
	for i := 0; i < b.N; i++ {
		Integrate(twoToX, "simp", benchIntFrom, benchIntTo, benchIntSlicesMedium, 1)
	}
}

func BenchmarkIntegrationSimpParallel1e3(b *testing.B) {
	for i := 0; i < b.N; i++ {
		Integrate(twoToX, "simp", benchIntFrom, benchIntTo, benchIntSlicesMedium, 8)
	}
}

func BenchmarkIntegrationTrapSequential1e3(b *testing.B) {
	for i := 0; i < b.N; i++ {
		Integrate(twoToX, "trap", benchIntFrom, benchIntTo, benchIntSlicesMedium, 1)
	}
}

func BenchmarkIntegrationTrapParallel1e3(b *testing.B) {
	for i := 0; i < b.N; i++ {
		Integrate(twoToX, "trap", benchIntFrom, benchIntTo, benchIntSlicesMedium, 8)
	}
}

func BenchmarkIntegrationMidpSequential1e3(b *testing.B) {
	for i := 0; i < b.N; i++ {
		Integrate(twoToX, "midp", benchIntFrom, benchIntTo, benchIntSlicesMedium, 1)
	}
}

func BenchmarkIntegrationMidpParallel1e3(b *testing.B) {
	for i := 0; i < b.N; i++ {
		Integrate(twoToX, "midp", benchIntFrom, benchIntTo, benchIntSlicesMedium, 8)
	}
}

func BenchmarkIntegrationSimpSequential10(b *testing.B) {
	for i := 0; i < b.N; i++ {
		Integrate(twoToX, "simp", benchIntFrom, benchIntTo, benchIntSlicesSmall, 1)
	}
}

func BenchmarkIntegrationSimpParallel10(b *testing.B) {
	for i := 0; i < b.N; i++ {
		Integrate(twoToX, "simp", benchIntFrom, benchIntTo, benchIntSlicesSmall, 8)
	}
}

func BenchmarkIntegrationTrapSequential10(b *testing.B) {
	for i := 0; i < b.N; i++ {
		Integrate(twoToX, "trap", benchIntFrom, benchIntTo, benchIntSlicesSmall, 1)
	}
}

func BenchmarkIntegrationTrapParallel10(b *testing.B) {
	for i := 0; i < b.N; i++ {
		Integrate(twoToX, "trap", benchIntFrom, benchIntTo, benchIntSlicesSmall, 8)
	}
}

func BenchmarkIntegrationMidpSequential10(b *testing.B) {
	for i := 0; i < b.N; i++ {
		Integrate(twoToX, "midp", benchIntFrom, benchIntTo, benchIntSlicesSmall, 1)
	}
}

func BenchmarkIntegrationMidpParallel10(b *testing.B) {
	for i := 0; i < b.N; i++ {
		Integrate(twoToX, "midp", benchIntFrom, benchIntTo, benchIntSlicesSmall, 8)
	}
}
