package integration

import (
	"math"
)

func integrateBasicSequential(f euclidianFunc, areaF areaFunc,
	from float64, to float64, slices int) float64 {

	sum := float64(0)
	spanX := to - from
	deltaX := spanX / float64(slices)

	for i := 0; i < slices; i++ {
		chunk_start := from + (float64(i) * deltaX)
		chunk_end := from + (float64(i+1) * deltaX)
		sum += areaF(f, chunk_start, chunk_end)
	}

	return math.Abs(sum)
}

func integrateSimpsonSequential(
	f euclidianFunc, from float64, to float64, slices int) float64 {

	samples := make([]float64, slices+1)
	spanX := to - from
	deltaX := spanX / float64(slices)
	for i := 0; i <= slices; i++ {
		x := from + (float64(i) * deltaX)
		samples[i] = f(x)
	}

	return math.Abs(
		(deltaX / 3) *
			sum(zipProduct(samples, simpsonCoefficients(slices+1))))
}
