package integration

import (
	"math"
)

func even(x int) bool {
	return x%2 == 0
}

func zipMap(f func(float64, float64) float64, arr1 []float64, arr2 []float64) []float64 {
	if len(arr1) != len(arr2) {
		panic("arrays must be same length")
	}

	new := make([]float64, len(arr1))
	for i := 0; i < len(new); i++ {
		new[i] = f(arr1[i], arr2[i])
	}
	return new
}

func channelToList(ch chan float64) []float64 {
	list := []float64{}
	for x := range ch {
		list = append(list, x)
	}
	return list
}

func zipProduct(arr1 []float64, arr2 []float64) []float64 {
	return zipMap(
		func(x float64, y float64) float64 { return x * y },
		arr1,
		arr2)
}

func sum(arr []float64) float64 {
	if len(arr) == 0 {
		return 0
	}

	return arr[0] + sum(arr[1:])
}

// Floating point equality
func approximately(x float64, y float64, epsilon float64) bool {
	// https://floating-point-gui.de/errors/comparison/
	// https://randomascii.wordpress.com/2012/02/25/comparing-floating-point-numbers-2012-edition/

	// Exactly equal
	if x == y {
		return true
	}

	// If x or y are exactly 0, we can just compare
	// the abs of the other operand to epsilon
	if x == 0 {
		return math.Abs(y) <= epsilon
	}

	if y == 0 {
		return math.Abs(x) <= epsilon
	}

	return math.Abs(x-y) <= math.Max(math.Abs(x), math.Abs(y))*epsilon
}
