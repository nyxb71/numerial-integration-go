package integration

import (
	"fmt"
	"testing"
)

func TestWithinMargin(t *testing.T) {
	var cases = []struct {
		a    float64
		b    float64
		e    float64
		want bool
	}{
		{a: 0, b: 0, e: 0, want: true},
		{a: 1, b: 1.0001, e: 1e-4, want: true},
		{a: 0, b: 7.105427357601002e-15, e: 1e-4, want: true},
	}

	for _, tc := range cases {
		t.Run(fmt.Sprintf("|%v - %v| < %v -> %v", tc.a, tc.b, tc.e, tc.want),
			func(t *testing.T) {
				if !approximately(tc.a, tc.b, tc.e) {
					t.Errorf(
						"Wrong output for\na:%v b:%v e:%v\n wanted: %v\n    got: %v\n",
						tc.a, tc.b, tc.e, tc.want, false)
				}
			})
	}
}
