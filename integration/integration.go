package integration

// A function that maps values of x to y on the euclidian plane
type euclidianFunc func(float64) float64

// A function that calculates the area of a euclidian function from f(a) to f(b)
type areaFunc func(euclidianFunc, float64, float64) float64

func areaTrapezoid(f euclidianFunc, from float64, to float64) float64 {
	deltaX := to - from
	return deltaX * ((f(from) + f(to)) / 2)
}

func areaMidpoint(f euclidianFunc, from float64, to float64) float64 {
	deltaX := to - from
	mid := from + (deltaX / 2)
	return deltaX * f(mid)
}

func deltaX(from float64, to float64, slices int) float64 {
	return (to - from) / float64(slices)
}

func Integrate(f euclidianFunc, method string,
	from float64, to float64, slices int, workers int) float64 {

	if from == to {
		return 0
	}

	if slices <= 0 {
		panic("slices must be > 0")
	}

	if workers <= 0 {
		panic("workers must be >= 1")
	}

	if workers == 1 {
		if method == "trap" {
			return integrateBasicSequential(f, areaTrapezoid, from, to, slices)
		}

		if method == "midp" {
			return integrateBasicSequential(f, areaMidpoint, from, to, slices)
		}

		if method == "simp" {
			return integrateSimpsonSequential(f, from, to, slices)
		}
	} else {
		if method == "trap" {
			return integrateBasicParallel(f, areaTrapezoid, from, to, slices, workers)
		}

		if method == "midp" {
			return integrateBasicParallel(f, areaMidpoint, from, to, slices, workers)
		}

		if method == "simp" {
			return integrateSimpsonParallel(f, from, to, slices, workers)
		}
	}

	return 0
}
