package integration

func simpsonCoefficient(cur int, max int) float64 {
	if even(max) {
		panic("max must be odd")
	}

	if max == 1 {
		return 1
	}

	if cur == 0 || cur+1 == max {
		return 1
	}

	if even(cur) {
		return 2
	}

	return 4
}

// Generates n simpson coeffients
func simpsonCoefficients(n int) []float64 {
	if even(n) {
		panic("n must be odd")
	}

	if n == 1 {
		return []float64{1}
	}

	list := make([]float64, n)
	for i := 0; i < n; i++ {
		list[i] = simpsonCoefficient(i, n)
	}

	return list
}
