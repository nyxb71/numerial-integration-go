package integration

import (
	"runtime"
	"sync"
)

func integrateBasicParallel(f euclidianFunc, af areaFunc,
	from float64, to float64, slices int, workers int) float64 {
	runtime.GOMAXPROCS(workers)

	results := make([]float64, workers)

	totalRange := to - from
	chunkSize := totalRange / float64(workers)
	slicesPerChunk := slices / workers

	var wg sync.WaitGroup
	wg.Add(workers)
	for i := 0; i < workers; i++ {
		chunkFrom := from + float64(i)*chunkSize
		chunkTo := chunkFrom + chunkSize
		go func(worker int, chunkFrom float64, chunkTo float64) {
			defer wg.Done()
			results[worker] = integrateBasicSequential(f, af, chunkFrom, chunkTo, slicesPerChunk)
		}(i, chunkFrom, chunkTo)
	}
	wg.Wait()

	runtime.GOMAXPROCS(runtime.NumCPU())
	return sum(results)
}

type Range struct {
	from int
	to   int
}

// Split a range into somewhat evenly sized chunks
// The last one may be larger if the range does not divide evenly
func chunkRange(start int, stop int, chunks int) []Range {
	ranges := make([]Range, chunks)

	totalRange := stop - start
	chunkSize := totalRange / chunks

	for i := 0; i < chunks; i++ {
		chunkStart := start + (chunkSize * i)
		chunkStop := chunkStart + chunkSize - 1

		ranges[i] = Range{from: chunkStart, to: chunkStop}
	}

	ranges[len(ranges)-1].to = stop

	return ranges
}

type SimpsonSample struct {
	target     float64
	coefficent float64
}

func integrateSimpsonParallel(
	f euclidianFunc, from float64, to float64, slices int, workers int) float64 {
	if slices < workers {
		panic("slices must be >= workers")
	}
	runtime.GOMAXPROCS(workers)

	numSamples := slices + 1

	work := make([]SimpsonSample, numSamples)

	dx := deltaX(from, to, slices)

	for i := 0; i < numSamples; i++ {
		work[i] = SimpsonSample{
			target:     float64(i) * dx,
			coefficent: simpsonCoefficient(i, numSamples)}
	}

	results := make([]float64, numSamples)

	var wg sync.WaitGroup
	wg.Add(workers)
	ranges := chunkRange(0, numSamples-1, workers)
	for i := 0; i < workers; i++ {
		go func(worker int) {
			defer wg.Done()

			for j := ranges[worker].from; j <= ranges[worker].to; j++ {
				results[j] = f(work[j].target) * work[j].coefficent
			}
		}(i)
	}
	wg.Wait()

	runtime.GOMAXPROCS(runtime.NumCPU())
	return (dx / 3) * sum(results)
}
